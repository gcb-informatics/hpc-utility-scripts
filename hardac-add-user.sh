#!/bin/bash
#
# This script adds a user to the HARDAC cluster.


# ----------------------
# --- default values ---

USERQUOTA=50G
CLUSTERGROUP=cluster
GPFS=/gpfs/fs1
GPFSHOME=${GPFS}/home
HOMEMODE=750


# -----------------
# --- functions ---

GracefulExit() {
  # use exit code is provided, otherwise 0
  [ ! -z $1 ] && EXIT=$1 || EXIT=0

  if [ $EXIT = 0 ]; then
    #QuietLog "`basename $0` finished successfully."
    exit 0
  else
    #WarnLog "`basename $0` finished with errors."
    exit $1
  fi

}

InfoLog() {
  # $1 is the message string
  echo "`date +%F\ %T` INFO: $1"
}

QuietLog() {
  # $1 is the message string
  [ ! $QUIET ] && echo "`date +%F\ %T` INFO: $1"
}

WarnLog() {
  # $1 is the message string
  echo "`date +%F\ %T` WARNING: $1"
}

ErrLog() {
  # $1 is the message string
  # optional $2 parameter is exit code
  echo "`date +%F\ %T` ERROR: $1" 1>&2

  if [ ! -z $2 ]; then
    EXIT=$2
    GracefulExit ${EXIT}
  fi
}


InvalidUsage() {
  echo "Usage: `basename $0` -p|--project|-a|--account <project/account> [-q|--quota <quota>] [--force] <NetID>"
  if [ ! -z $1 ]; then
    EXIT=$1
    GracefulExit ${EXIT}
  fi
}



# ------------------------------------
# --- parse command-line arguments ---

while [ 0 -lt $# ]; do
  case "${1}" in
    --help|-h)
      InvalidUsage 0
    ;;

    -q|--quiet)
      QUIET=true
    ;;

    -p|--project|-a|--account)
      shift
      ACCOUNT="${1}"
    ;;

    -q|--quota)
      shift
      USERQUOTA="${1}"
    ;;

    --force)
      FORCE=true
    ;;

    *)
      # check if a user has already been specified
      if [ ! -z "${NETID}" ]; then
        ErrLog "Extraneous option: ${1}"
        InvalidUsage 1
      fi

      if id ${1} >/dev/null 2>&1; then
        # valid user
        NETID="${1}"
      else
        # invalid user
        ErrLog "Unknown user or option: ${1}" 1
      fi
    ;;

  esac
  shift
done



# ------------------
# --- validation ---

# make sure a user was specified
if [ -z "${NETID}" ]; then
  ErrLog "Please specify a user"
  InvalidUsage 1
fi

# make sure a project/account was specified
if [ -z "${ACCOUNT}" ]; then
  ErrLog "Please specify a project/account"
  InvalidUsage 1
fi

# check if the user already exists
if [ -d "${GPFSHOME}/${NETID}" ]; then
  ErrLog "User already exists" 5
fi

# check if user is a member of the cluster group
if ! id -Gn ${NETID} |grep -w ${CLUSTERGROUP} >/dev/null 2>&1; then
  if [ ${FORCE} ]; then
    WarnLog "User is not a member of ${CLUSTERGROUP}, will continue since --force was specified"
  else
    ErrLog "User must already be a member of group ${CLUSTERGROUP} (use --force to override)" 2
  fi
fi

# check if project/account is a real UNIX group
if ! getent group "${ACCOUNT}" >/dev/null; then
  ErrLog "Project/account UNIX group does not exist: ${ACCOUNT}" 6
fi

# check if project/account has been added to Slurm account manager
if [ $(sacctmgr -n list account "${ACCOUNT}" | wc -l) -eq 0 ]; then
  ErrLog "Project/account does not exist in Slurm accounting database: ${ACCOUNT}"
  echo; echo "Add account ${ACCOUNT} to Slurm accounting database?"; echo
  echo -n " (y/n) "
  read x
  if [[ "$x" =~ "y" ]]; then
    sacctmgr add account ${ACCOUNT} || ErrLog "Error adding account to Slurm accounting database ($?)"
  else
    ErrLog "Cannot proceed, try specifying a different account or add the account to Slurm accounting database"
  fi
else
  QuietLog "Account ${ACCOUNT} exists in Slurm accounting database."
fi



# -----------------------------
# --- create user resources ---

# create the user's homedir and set permission mode
QuietLog "Creating home directory..."
mkdir "${GPFSHOME}/${NETID}" || ErrLog "Error creating homedir ($?)"

QuietLog "Setting home directory permissions..."
chmod ${HOMEMODE} "${GPFSHOME}/$NETID" || ErrLog "Error setting homedir owner ($?)"

# set the user quota
QuietLog "Setting GPFS quota..."
mmsetquota -u ${NETID} -h ${USERQUOTA} ${GPFSHOME} || ErrLog "Error setting user quota ($?)"

# create the skel files
QuietLog "Copying default files..."
rsync -a /etc/skel/ "${GPFSHOME}/$NETID/" || ErrLog "Error copying user skel files ($?)"

# set file ownership recursively: owner = user, group = user's primary group
QuietLog "Setting home directory ownership..."
chown -R $NETID.$(id -gn $NETID) "${GPFSHOME}/$NETID" || ErrLog "Error setting user permissions ($?)"


# create user in Slurm accounting database
QuietLog "Adding user to Slurm accounting database"
sacctmgr add user ${NETID} DefaultAccount=${ACCOUNT} || ErrLog "Error adding user in Slurm accounting database ($?)"



# -----------------------
# --- exit gracefully ---

QuietLog "Done."
GracefulExit
