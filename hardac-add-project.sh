#!/bin/bash
#
# This script adds a data project to the HARDAC cluster


# ----------------------
# --- default values ---

PROJECTQUOTA=3T
GPFS=/gpfs/fs1
GPFSDATA=${GPFS}/data
GPFSDEV=gpfs.fs1
FILESETPREFIX=data_
PROJECTMODE=2770
PROJECTUSER=root


# -----------------
# --- functions ---

GracefulExit() {
  # use exit code is provided, otherwise 0
  [ ! -z $1 ] && EXIT=$1 || EXIT=0

  if [ $EXIT = 0 ]; then
    #QuietLog "`basename $0` finished successfully."
    exit 0
  else
    #WarnLog "`basename $0` finished with errors."
    exit $1
  fi

}

InfoLog() {
  # $1 is the message string
  echo "`date +%F\ %T` INFO: $1"
}

QuietLog() {
  # $1 is the message string
  [ ! $QUIET ] && echo "`date +%F\ %T` INFO: $1"
}

WarnLog() {
  # $1 is the message string
  echo "`date +%F\ %T` WARNING: $1"
}

ErrLog() {
  # $1 is the message string
  # optional $2 parameter is exit code
  echo "`date +%F\ %T` ERROR: $1" 1>&2

  if [ ! -z $2 ]; then
    EXIT=$2
    GracefulExit ${EXIT}
  fi
}


InvalidUsage() {
  echo "Usage: `basename $0` -g|--group <project_unix_group> [--quota <quota>] <project>"
  if [ ! -z $1 ]; then
    EXIT=$1
    GracefulExit ${EXIT}
  fi
}



# ------------------------------------
# --- parse command-line arguments ---

while [ 0 -lt $# ]; do
  case "${1}" in
    --help|-h)
      InvalidUsage 0
    ;;

    -q|--quiet)
      QUIET=true
    ;;

    --quota)
      shift
      PROJECTQUOTA="${1}"
    ;;

    -g|--group)
      shift
      PROJECTGROUP="${1}"
    ;;

    *)
      # check if a project has already been specified
      if [ ! -z "${PROJECT}" ]; then
        ErrLog "Extraneous option: ${1}"
        InvalidUsage 1
      else
        PROJECT="${1}"
      fi
    ;;

  esac
  shift
done



# ------------------
# --- validation ---

# make sure a project and project group were specified
if [ -z "${PROJECT}" ] || [ -z "${PROJECTGROUP}" ]; then
  ErrLog "Please specify a project and project group"
  InvalidUsage 1
fi

# check if the project already exists
if [ -d "${GPFSDATA}/${PROJECT}" ]; then
  ErrLog "Project already exists" 5
fi

# verify the project group is a valid UNIX group
if ! getent group ${PROJECTGROUP} >/dev/null 2>&1; then
  ErrLog "The project group specified '${PROJECTGROUP}' is not a valid UNIX group" 2
fi


# --------------------------------
# --- create project resources ---

# concatenate the fileset name
PROJECTFILESET="${FILESETPREFIX}${PROJECT}"

# create and link/junction/mount the project fileset
/usr/lpp/mmfs/bin/mmcrfileset ${GPFSDEV} ${PROJECTFILESET} || ErrLog "Error creating project fileset ($?)"
/usr/lpp/mmfs/bin/mmlinkfileset ${GPFSDEV} ${PROJECTFILESET} -J "${GPFSDATA}/${PROJECT}" || ErrLog "Error linking the project fileset ($?)"

# set the group quota for the project
#/usr/lpp/mmfs/bin/mmsetquota -j ${PROJECTFILESET} -h ${PROJECTQUOTA} ${GPFS} || ErrLog "Error setting project quota ($?)"
/usr/lpp/mmfs/bin/mmsetquota ${GPFSDEV}:${PROJECTFILESET} --block ${PROJECTQUOTA}:${PROJECTQUOTA} || ErrLog "Error setting project quota ($?)"

# set permissions
chmod ${PROJECTMODE} "${GPFSDATA}/${PROJECT}" || ErrLog "Error setting project dir owner ($?)"

# set permissions recursively
chown -R ${PROJECTUSER}.${PROJECTGROUP} "${GPFSDATA}/${PROJECT}" || ErrLog "Error setting project dir permissions ($?)"



# -----------------------
# --- exit gracefully ---

GracefulExit
