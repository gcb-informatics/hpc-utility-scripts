#!/bin/bash
HOME=/hpchome
GCB_HOMES=($HOME/igsp1 $HOME/igps2 $HOME/igspncbc $HOME/gcb)
START=2016-01-01
END=now
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

for home in "${GCB_HOMES[@]}"
do
  for user in $home/*
  do
    user=${user%*/}
    if [[ $(sacct -u ${user##*/} -o jobid -n -S $START -E $END 2>/dev/null) ]]; then
      echo -e "${GREEN}User ${user##*/} is active${NC}"
    else
      echo -e "${RED}User ${user##*/} is not active${NC}"
    fi
  done
done
